import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  data: any;
  headers: any;
  parken: any;
  loading: any;
  counter: any;
  count: any;
  status: any;
  parknride: any;
  gesamtFrei: any;
  gesamtKapazitaet: any;

  constructor(public navCtrl: NavController, private http: HttpClient, private loadingCtrl: LoadingController) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.data = "";
    this.parken = [];
    this.parknride  = [];
    this.counter = 0;
    this.count = 0;
    this.status = "";

    this.loading = this.loadingCtrl.create({
      content: "Daten werden geladen..."
    })
    this.loading.present();

    this.getParkingData();
  }

  timeout(){
    setTimeout(()=>{
      this.getParkingData();
    }, 30000);
  }

  getParkingData(){
    this.http.get("https://w3-development.de/parken/get.php", {headers: this.headers}).subscribe((data:any) =>{
      if(this.counter == 0){
        this.loading.dismiss();
        this.counter == 1;
      }
      let maxCapacity = 0;
      let freiInsgesamt = 0;
      this.parken = [];
      this.parknride = [];
      data.einrichtung.forEach((item:any) =>{
        let object = Object();
        object.name = item.stammdaten.bezeichnung;
        object.kapazitaet = item.stammdaten.kapazitaet;
        object.bereich = item.stammdaten.bereich;
        object.frei = item.belegung["@attributes"].frei;
        object.tendenz = item.belegung["@attributes"].tendenz;

        // status
        let frei = item.belegung["@attributes"].frei;
        let kapazitaet = item.stammdaten.kapazitaet;
        let tendenz = item.belegung["@attributes"].tendenz;

        if(frei < (0.1 * kapazitaet)){
          object.status = "red";
        }else if(frei < (0.2 * kapazitaet)){
          object.status = "yellow";
        }else{
          object.status = "green";
        }

        if(object.name != "Byk Gulden Str." && object.name != "Seerheincenter" && object.name != "Bodenseeforum"){
          maxCapacity = maxCapacity + parseInt(kapazitaet);
          freiInsgesamt = freiInsgesamt + parseInt(frei);
          // tendenz
          if(tendenz == "zunehmend"){
            this.count++;
          }else if(tendenz == "abnehmend"){
            this.count--;
          }

          this.parken.push(object);
        }else{
          if(object.name != "Byk Gulden Str."){
            this.parknride.push(object);
          }

        }

      })
      console.log("maxCapacity: "+maxCapacity);
      console.log("freiInsgesamt: "+freiInsgesamt);
      this.gesamtKapazitaet = maxCapacity;
      this.gesamtFrei = freiInsgesamt;
      if(freiInsgesamt < (0.1 * maxCapacity)){
        this.status = "red";
      }else if(freiInsgesamt < (0.2 * maxCapacity)){
        this.status = "yellow";
      }else{
        this.status = "green";
      }
      this.timeout();
    }, error =>{
      if(this.counter == 0){
        this.loading.dismiss();
        this.counter == 1;
      }
      console.log(error);
      this.timeout();
    });
  }

}
